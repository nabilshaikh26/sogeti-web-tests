/* eslint-disable no-console */
const { exec } = require('child_process');

let tags = '';
const environment = process.env.ENV;

switch (environment) {
  case 'dev':
    tags = "TAGS='@API'";
    console.log(`Tests running on ${environment} environment by referring ${tags}`);
    break;
  case 'qa':
    tags = "TAGS='@Smoke'";
    console.log(`Tests running on ${environment} environment by referring ${tags}`);
    break;
  default:
    console.log('Running all tests on staging');
}

const command = `./node_modules/.bin/cypress run -e ${tags} GLOB='cypress/e2e/specs/**/*.feature'`;
const ls = exec(`${command}`);

ls.stdout.on('data', (data) => {
  console.log(`stdout: ${data}`);
});

ls.stderr.on('data', (data) => {
  console.error(`stderr: ${data}`);
});

ls.on('close', (code) => {
  console.log(`child process exited with code ${code}`);
});
