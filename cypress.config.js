/* eslint-disable no-dupe-keys */
/* eslint-disable linebreak-style */
/* eslint-disable import/no-unresolved */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable linebreak-style */
const { defineConfig } = require('cypress');
const preprocessor = require('@badeball/cypress-cucumber-preprocessor');
const browserify = require('@badeball/cypress-cucumber-preprocessor/browserify');
const allureWriter = require('@shelex/cypress-allure-plugin/writer');
const { configureAllureAdapterPlugins } = require('@mmisty/cypress-allure-adapter/plugins');

async function setupNodeEvents(on, config) {
  await preprocessor.addCucumberPreprocessorPlugin(on, config);
  on('file:preprocessor', browserify.default(config));
  allureWriter(on, config);
  configureAllureAdapterPlugins(on, config);
  return config;
}

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    setupNodeEvents,
    baseUrl: 'https://www.sogeti.com',
    specPattern: 'cypress/e2e/specs/{ui,api}/*.feature',
    viewportWidth: 1280,
    viewportHeight: 800,
    pageLoadTimeout: 200000,
    defaultCommandTimeout: 10000,
    retries: 2,
    video: false,
    allure: true,
  },
});
