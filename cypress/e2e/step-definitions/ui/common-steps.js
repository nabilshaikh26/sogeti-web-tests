import { Given } from '@badeball/cypress-cucumber-preprocessor';

/**
  * @desc Generic steps
*/

Given('user visits {string} page on {string} device', (pageName, device) => {
  switch (device) {
    case 'mobile':
      cy
        .log('Opening mobile...');
      cy
        .viewport('iphone-6'); // {width-375 & height-667}
      cy
        .setCookie('ConsentCookie', 'required:1,functional:0,analytic:0');
      cy
        .visit(pageName);
      break;
    case 'tablet':
      cy
        .log('Opening tablet...');
      cy
        .viewport('ipad-2'); // {width-768 & height-1024}
      cy
        .setCookie('ConsentCookie', 'required:1,functional:0,analytic:0');
      cy
        .visit(pageName);
      break;
    case 'desktop':
      cy
        .log('Opening desktop...');
      cy
        .viewport('macbook-15'); // {width-1440 & height-900}
      cy
        .setCookie('ConsentCookie', 'required:1,functional:0,analytic:0');
      cy
        .visit(pageName);
      break;
    default:
      cy
        .log('Starting test in default viewport 1280x800');
  }
});
