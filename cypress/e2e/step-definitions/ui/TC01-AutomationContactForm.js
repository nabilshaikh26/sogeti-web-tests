/* eslint-disable linebreak-style */
import { When, Then } from '@badeball/cypress-cucumber-preprocessor';
import Header from '../../ui-identifiers/Header';
import Automation from '../../ui-identifiers/Automation';

const header = new Header();
const automation = new Automation();

// Scenario-1

When("user hover over the 'Services' menu present at the top of the screen and selects the {string} option", (serviceOption) => {
  header
    .getDesktopServicesMenu()
    .invoke('mouseover')
    .find('.subMenuLink')
    .contains(serviceOption)
    .click();
});

Then("user should navigate to the {string} page successfully", (automation) => {
  cy
    .url()
    .should('contain', Cypress._.lowerCase(automation))
  cy
    .contains(automation)
});

When("user should see both 'Services' and 'Automation' options to be selected indicating about their current selection", (colour) => {
  header
    .getDesktopServicesMenu()
    .then((element) => {
      expect(element).to.have.class('selected');
      expect(element).to.contains.text('Services')
    });
  header
    .getDesktopServicesMenu()
    .find('.wrapper > span')
    .should('have.css', 'color', 'rgb(255, 48, 76)');

  header
    .getDesktopAutomationSubmenu()
    .then((element) => {
      expect(element.text().trim()).to.eq('Automation');
      expect(element).to.have.class('selected');
    });
  });

// Scenario-2

When('user expand the hamburger menu present on the top-right corner of screen', () => {
  header
    .getHamburgerMenu()
    .click();
});

When("user selects the 'Automation' option present under the 'Services' menu", () => {
  header
    .getMobileServicesMenu()
    .click();
  cy
    .get('.expanded > .wrapper > .mega-navbar > .level1 > :nth-child(4) > a')
    .click();
});

When("user should find 'Automation' option to be selected present under the hamburger menu indicating about their current selection", () => {
  header
    .getHamburgerMenu()
    .click();

  header
    .getMobileServicesMenu()
    .then((element) => {
      expect(element).to.have.class('selected');
      expect(element).to.contains.text('Services')
    });

  header
    .getMobileAutomationSubmenu()
    .then((element) => {
      expect(element.text().trim()).to.eq('Automation');
      expect(element).to.have.class('selected');
    })
  });

// Scenario-3

When('user sees the {string} form present at the bottom of the page', (contactUsTitle) => {
  cy
    .contains(contactUsTitle);
});

When('user performs the following actions to fill the complete form', () => {
});

When('enters First Name as {string}', (fname) => {
  automation
    .contactFirstName()
    .type(fname, {force: true});
});

When('enters Last Name as {string}', (lname) => {
  automation
    .contactLastName()
    .type(lname, {force: true});
});

When("enters Email as {string}", (email) => {
  automation
    .contactEmail()
    .type(email, {force: true});
});

When('enters Phone number as {string}', (phone) => {
  automation
    .contactPhoneNumber()
    .type(phone, {force: true});
});

When('enters Company as {string}', (company) => {
  automation
    .contactCompanyName()
    .type(company, {force: true})
});

When('selects Country as {string}', (country) => {
  cy
    .get('select')
    .select(country, {force: true})
});

When('enters Message as {string}', (message) => {
  automation
    .contactMessage()
    .type(message, {force: true});
});

When("checks the 'I agree' checkbox to indicate the acceptance of T&C", () => {
  automation
    .contactAgreementCheckbox()
    .check({force: true});
});

When("confirms the reCAPTCHA", () => {
});

When("user clicks on Submit button", () => {
  automation
    .contactSubmitButton()
    .click();
});

When("user should see the message as {string} indicating the successful form submission", (successMessage) => {
  cy
    .contains(successMessage);
});

// Scenario-4

When("user expands the Worldwide dropdown list present in the page header", () => {
  header
    .getWorldwideDropdown()
    .click();
});

Then("the country dropdown list should be displayed", () => {
  header
    .getWorldwideCountryList()
    .should('be.visible');
});

When("each country specific Sogeti links should be working", () => {
  header
    .getWorldwideCountryList()
    .its('length')
    .then(length => {
      for(let i=0; i<length; i++){
        header.getWorldwideCountryList().eq(i).invoke('attr', 'href').then((url) => {
          cy.request(url).then((response) => {
            expect(response.status).to.eq(200);
          })
        });
      };
    });
});

