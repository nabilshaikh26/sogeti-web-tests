import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';

let baseUrl;
Given('the base url {string}', (url) => {
  baseUrl = url;
});

// Scenario-1

When('I make HTTP {string} request to an endpoint {string}', (method, endpoint) => {
  cy
    .request(baseUrl + endpoint)
    .as('apiResponse');
});

Then('response status should equals {int} where the response time should be below {string}', (statusCode, responseTime) => {
  responseTime = parseInt(responseTime) * 1000
  cy
    .get('@apiResponse').then((response) => {
      expect(response.status).to.eq(statusCode)
      expect(response.duration).to.be.lessThan(responseTime);
    });
});

Then('verify response {string} to be of {string}', (key, value) => {
  cy
    .get('@apiResponse')
    .then((response) => {
      expect(response.headers[key]).to.contains(value);
    });
});

Then('response payload should have the following properties', () => {});

Then('verify {string} key should equal value {string}', (key, value) => {
  cy
    .get('@apiResponse')
    .then((response) => {
      const body = response.body
      expect(body[key]).to.equal(value)
    });
});

Then('verify postal code {string} should have the place name {string}', (postalCode, value) => {
  cy
    .get('@apiResponse')
    .then((response) => {
      const place = response.body.places.find(place => place['post code'] === postalCode);
      expect(place['place name']).to.eq(value)
    });
});

// Scenario-2

Then('verify {string} shows appropriate {string} result', (key, value) => {
  cy
    .get('@apiResponse')
    .then((response) => {
      const place = response.body.places.find(place => place[key]);
      expect(place[key]).to.equal(value);
    });
});