Feature: CAPTCHA API

As a tester, I want to verify the postal details of various countries & states using the Zippopotamus API.

    Background: Initialize the Zippopotamus baseurl
        Given the base url 'http://api.zippopotam.us'
    
    @API
    Scenario: Verify the Deutschland's Stuttgart city postal details in response payload
        When I make HTTP 'GET' request to an endpoint '/de/bw/stuttgart'
        Then response status should equals 200 where the response time should be below '1s'
        * verify response 'content-type' to be of 'json'
        * verify 'country' key should equal value 'Germany'
        * verify 'state' key should equal value 'Baden-Württemberg'
        * verify postal code '70597' should have the place name 'Stuttgart Degerloch'
    
    @API
    Scenario Outline: Verify the response payload based on the postal code
        When I make HTTP 'GET' request to an endpoint '/<country>/<postal-code>'
        Then response status should equals 200 where the response time should be below '1s'
        * verify response 'content-type' to be of 'json'
        * verify 'place name' shows appropriate '<place>' result 

        Examples:
        | country | postal-code | place         |
        | us      | 90210       | Beverly Hills |
        | us      | 12345       | Schenectady   |
        | ca      | B2R         | Waverley      |