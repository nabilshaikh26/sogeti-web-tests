Feature: Contact Page for Automation Customers

As a product manager, I need a contact form to be implemented on the Automation page 
so that our automation experts can resolve the customer's queries and help them to optimizes their processes and adds substantial business value to their operations.

@1 @Smoke
Scenario: Navigate to automation page on desktop viewport
    Given user visits '/' page on 'desktop' device
    When user hover over the 'Services' menu present at the top of the screen and selects the 'Automation' option
    Then user should navigate to the 'Automation' page successfully
    And user should see both 'Services' and 'Automation' options to be selected indicating about their current selection

@2
Scenario: Navigate to automation page on mobile viewport
    Given user visits '/' page on 'mobile' device
    And user expand the hamburger menu present on the top-right corner of screen
    When user selects the 'Automation' option present under the 'Services' menu
    Then user should navigate to the 'Automation' page successfully
    And user should find 'Automation' option to be selected present under the hamburger menu indicating about their current selection

@3
# Commenting few steps below to avoid CAPTCHA interaction
Scenario Outline: Submit the contact form related to the Automation service
    Given user visits '/services/automation' page on '<viewport>' device
    And user sees the 'Contact us' form present at the bottom of the page
    When user performs the following actions to fill the complete form
    * enters First Name as 'Hello'
    * enters Last Name as 'World'
    * enters Email as 'helloworld@gmail.com'
    * enters Phone number as '+91-9090909090'
    * enters Company as 'XYZ Company'
    * selects Country as 'Germany'
    * enters Message as 'This is my query!'
    * checks the 'I agree' checkbox to indicate the acceptance of T&C
    # * confirms the reCAPTCHA
    And user clicks on Submit button
    # Then user should see the message as 'Thank you for contacting us.' indicating the successful form submission

        Examples:
        | viewport | 
        | desktop  |
        | mobile   |

@4
Scenario Outline: Country specific Sogeti links are working and not broken
    Given user visits '/services/automation' page on '<viewport>' device
    When user expands the Worldwide dropdown list present in the page header
    Then the country dropdown list should be displayed
    And each country specific Sogeti links should be working

        Examples:
        | viewport | 
        | desktop  |
        | mobile   |