class Automation {
    contactFirstName() {
      return cy.get('#4ff2ed4d-4861-4914-86eb-87dfa65876d8');
    }

    contactLastName() {
      return cy.get('#11ce8b49-5298-491a-aebe-d0900d6f49a7');
    }

    contactEmail() {
      return cy.get('#056d8435-4d06-44f3-896a-d7b0bf4d37b2');
    }

    contactPhoneNumber() {
      return cy.get('#755aa064-7be2-432b-b8a2-805b5f4f9384');
    }

    contactCompanyName() {
      return cy.get('#703dedb1-a413-4e71-9785-586d609def60');
    }

    contactMessage() {
      return cy.get('#88459d00-b812-459a-99e4-5dc6eff2aa19');
    }

    contactAgreementCheckbox() {
      return cy.get('#__field_1239350');
    }

    contactSubmitButton() {
      return cy.get('#b35711ee-b569-48b4-8ec4-6476dbf61ef8');
    }
  }
  
  export default Automation;