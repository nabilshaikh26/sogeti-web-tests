class Header {
    getDesktopServicesMenu() {
      return cy.get('#main-menu > .clearfix').find('.has-children').eq(3);
    }

    getDesktopAutomationSubmenu() {
      return cy.get('#main-menu > .clearfix > .level2 > .mega-navbar').find('li').eq(3);
    }

    getMobileServicesMenu() {
      return cy.get('#main-menu-mobil > .clearfix').find('.has-children').eq(3);
    }

    getMobileAutomationSubmenu() {
      return cy.get('#main-menu-mobil > .clearfix').find('.has-children').eq(3).find('li').eq(3);
    }

    getHamburgerMenu() {
      return cy.get('.navbar-toggle');
    }

    getWorldwideDropdown() {
      return cy.get('.sprite-globe');
    }

    getWorldwideCountryList() {
      return cy.get('.country-list > ul > li >');
    }
  }
  
  export default Header;